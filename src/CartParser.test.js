import CartParser from './CartParser';
import {readFileSync} from "fs";

let parser, parse, validate, parseLine, columnLength, calcTotal;

beforeEach(() => {
	parser = new CartParser();
	columnLength = parser.schema.columns.length;
	parse = parser.parse.bind(parser);
	validate = parser.validate.bind(parser);
	parseLine = parser.parseLine.bind(parser);
	calcTotal = parser.calcTotal;
});

describe('CartParser - unit tests', () => {
	const wrongCart = readFileSync('samples/cart-test.csv', 'utf-8', 'r');

	it('validate should return an array of validation errors that contain column, row, message and type', () => {
		const errors = validate('samples/cart-test.csv');
		const expectedError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: expect.any(String)
		}

		for (let error of errors) {
			expect(error).toEqual(expectedError);
		}
		expect(errors.length).toBeGreaterThan(0);
	});

	it('validate should return an array which contains a header error if the first line is not valid', () => {
		const headerError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: 'header'
		}

		expect(validate(wrongCart)).toContainEqual(headerError);
		expect(validate('Lorem ipsum,Price,Quantity')).toContainEqual(headerError);
	})

	it('validate should return an array which contains a row error if the number of columns is not 3', () => {
		const rowError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: 'row'
		}
		expect(validate('Product name,Price,Quantity\n28.72,10')).toContainEqual(rowError);
	})

	it('validate should return an array which contains a cell error if name is an empty string', () => {
		const cellError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: 'cell'
		}
		expect(validate('Product name,Price,Quantity\n ,28.72,10')).toContainEqual(cellError);
	})

	it('validate should return an array which contains a cell error if price is not a positive number', () => {
		const cellError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: 'cell'
		}

		expect(validate('Product name,Price,Quantity\nMollis consequat,9.002,-3')).toContainEqual(cellError);
		expect(validate('Product name,Price,Quantity\nMollis consequat,9.002,NaN')).toContainEqual(cellError);
		expect(validate('Product name,Price,Quantity\nMollis consequat,9.002,abc')).toContainEqual(cellError);
	})

	it('validate should return an array which contains a cell error if quantity is not a positive number', () => {
		const cellError = {
			column: expect.any(Number),
			message: expect.any(String),
			row: expect.any(Number),
			type: 'cell'
		}

		expect(validate('Product name,Price,Quantity\nMollis consequat,-5,1')).toContainEqual(cellError);
		expect(validate('Product name,Price,Quantity\nMollis consequat,ab,1')).toContainEqual(cellError);
		expect(validate('Product name,Price,Quantity\nMollis consequat,NaN,1')).toContainEqual(cellError);
	})

	it('validate should return an empty array if validation is successful', () => {
		const cart = readFileSync('samples/cart.csv', 'utf-8', 'r');
		expect(validate(cart)).toEqual([]);
	});


	it('parseLine should return an object which contains id, name, price and quantity', () => {
		const itemExpected = {
			id: expect.any(String),
			name: expect.any(String),
			price: expect.any(Number),
			quantity: expect.any(Number)
		}

		expect(parseLine('Mollis consequat,9.00,2')).toEqual(itemExpected);
	})

	it('calcTotal should return the total price of items', () => {
		const cart = JSON.parse(readFileSync('samples/cart.json', 'utf-8', 'r'));
		expect(calcTotal(cart.items)).toBeCloseTo(cart.total, 2);
	})
});

describe('CartParser - integration test', () => {
	it('parse should return a JSON object which contains items and total or throw error if validation failed', () => {
		const expected = {
			items: expect.any(Array),
			total: expect.any(Number)
		}
		const itemExpected = {
			id: expect.any(String),
			name: expect.any(String),
			price: expect.any(Number),
			quantity: expect.any(Number)
		}

		const result = parse('samples/cart.csv');

		expect(result).toEqual(expect.objectContaining(expected));
		expect(result.total).toBeCloseTo(348.32, 2);
		for (let item of result.items) {
			expect(item).toEqual(expect.objectContaining(itemExpected));
		}

		const spy = jest.spyOn(console, 'error');
		spy.mockImplementation(() => {});
		expect(() => parse('samples/cart-test.csv')).toThrowError(new Error('Validation failed!'));
		spy.mockRestore();
	})
});